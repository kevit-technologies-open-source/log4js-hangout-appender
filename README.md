Log4js Node Hangout Appender
--------------------------------

Sends log events to a hangout room or group. This is an appender for use with log4js.

## Instructions:
## Install the package:
```
npm i @kevit/log4js-hangout
```

## Configuration

* `type` - `@kevit/log4js-hangout`
* `webhookURL` - Your hangout incoming webhhok URL or provide `token,space and key`
* `token` - `string` - your Hangout Incoming webhook token
* `space` - `string` - the room or group to send log messages
* `key` - `string` - your Hangout Incoming webhook key
* `layout` - `object` (optional, defaults to `patternLayout` with pattern  ``` `%p` %c%n%m ```) - the layout to use for the message (see [layouts](https://log4js-node.github.io/log4js-node/layouts.html)).
* `sendInterval` - `number` -(Optional, defaults to 24 hours) For these many milliseconds, the logs will be sent to same thread, and after it in new thread
## Example

```javascript
log4js.configure({
    appenders: {
        hangoutAlert: {
            type: '@kevit/log4js-hangout',
            space: '****',
            key:'****',
            token:'****'
        }
    },
    categories: {default: {appenders: ['hangoutAlert'], level: 'warn'}}
});
```
Without webhookUrl 

#### With webhook url
```javascript
log4js.configure({
    appenders: {
        hangoutAlert: {
            type: '@kevit/log4js-hangout',
            webhookUrl: 'https://chat.googleapis.com/v1/spaces/*****/messages?key=*****&token=***'
        }
    },
    categories: {default: {appenders: ['hangoutAlert'], level: 'warn'}}
});
```
This configuration will send all warn (and above) messages to the respective hangout room.


#### With multiple appenders
```javascript
log4js.configure({
	appenders: {
		out: {type: 'stdout'},
		allLogs: {type: 'file', filename: 'all.log', maxLogSize: 10485760, backups: 10, compress: true},
		outFilter: {
			type: 'logLevelFilter', appender: 'out', level: process.env.LOG_LEVEL || 'all'
		},
		hangout: {
		    type: '@kevit/log4js-hangout',
		    webhookUrl: 'https://chat.googleapis.com/v1/spaces/*****/messages?key=*****&token=***'
		    },
		hangoutFilter: {
			type: 'logLevelFilter', appender: 'hangout', level: process.env.ALERT_LOG_LEVEL || 'warn'
		}
	},
	categories: {
		default: {appenders: ['allLogs','outFilter', 'hangoutFilter']}
	}
});
```
This configuration displays use of multiple appenders.

- `outFilter`: Push log in **stdout** with filter `LOG_LEVEL` set in environment, if not set then `all` levels
- `hangoutFilter`: Push log in **hangout-webhook** with filter `ALERT_LOG_LEVEL` set in environment, if not set then `warn` levels

#### With sendInterval
```javascript
log4js.configure({
    appenders: {
        hangoutAlert: {
            type: '@kevit/log4js-hangout',
            webhookUrl: 'https://chat.googleapis.com/v1/spaces/*****/messages?key=*****&token=***',
            sendInterval: 2*60*60*1000

        }
    },
    categories: {default: {appenders: ['hangoutAlert'], level: 'warn'}}
});
```
This configuration sets new thread for logging of messages after every 2 hours.

### Note: 
If you will send more content than 4096 characters in a single message, than it will be stripped to 4096 characters. 



For more configuration, see [log4js](https://log4js-node.github.io/log4js-node)
